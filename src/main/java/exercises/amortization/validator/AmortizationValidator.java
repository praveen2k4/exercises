package exercises.amortization.validator;

/**
 * A validator interface with required validations to build an amortization
 * @author pnuthulapati
 */
public interface AmortizationValidator {
 public boolean isValidBorrowAmount(double amount);
 public double[] getBorrowAmountRange();
 public boolean isValidAPRValue(double rate);
 public double[] getAPRRange();
 public boolean isValidTerm(int years);
 public int[] getTermRange();
}
