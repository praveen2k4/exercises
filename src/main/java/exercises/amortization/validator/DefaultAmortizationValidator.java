package exercises.amortization.validator;


/**
 * A default implementation for amortization validation
 *
 */
public class DefaultAmortizationValidator implements AmortizationValidator {

	private static final double[] borrowAmountRange = new double[] { 0.01d, 1000000000000d };
	private static final double[] aprRange = new double[] { 0.000001d, 100d };	
	private static final int[] termRange = new int[] { 1, 1000000 };
	
	@Override
	public boolean isValidBorrowAmount(double amount) {
		double range[] = getBorrowAmountRange();
		return ((range[0] <= amount) && (amount <= range[1]));
	}

	@Override
	public boolean isValidAPRValue(double rate) {
		double range[] = getAPRRange();
		return ((range[0] <= rate) && (rate <= range[1]));
	}

	@Override
	public boolean isValidTerm(int years) {
		int range[] = getTermRange();
		return ((range[0] <= years) && (years <= range[1]));
	}

	@Override
	public double[] getBorrowAmountRange() {
		return borrowAmountRange;
	}

	@Override
	public double[] getAPRRange() {
		return aprRange;
	}

	@Override
	public int[] getTermRange() {
		return termRange;
	}

}
