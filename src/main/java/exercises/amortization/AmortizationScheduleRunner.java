package exercises.amortization;

import static exercises.amortization.ConsoleInputOutput.print;
import static exercises.amortization.ConsoleInputOutput.readLine;

import java.io.IOException;

import exercises.amortization.validator.AmortizationValidator;
import exercises.amortization.validator.DefaultAmortizationValidator;

/**
 * Runs the AmortizationSchedule Program
 * @author pnuthulapati
 *
 */
public class AmortizationScheduleRunner {
		
	public static void main(String [] args) {
		AmortizationValidator validator =new DefaultAmortizationValidator();
		String[] userPrompts = {
				"Please enter the amount you would like to borrow: ",
				"Please enter the annual percentage rate used to repay the loan: ",
				"Please enter the term, in years, over which the loan is repaid: "
		};
		
		String line = "";
		double amount = 0;
		double apr = 0;
		int years = 0;
		
		for (int i = 0; i< userPrompts.length; ) {
			String userPrompt = userPrompts[i];
			try {
				line = readLine(userPrompt);
			} catch (IOException e) {
				print("An IOException was encountered. Terminating program.\n");
				return;
			}
			
			boolean isValidValue = true;
			try {
				switch (i) {
				case 0:
					amount = Double.parseDouble(line);
					if (validator.isValidBorrowAmount(amount) == false) {
						isValidValue = false;
						double range[] = validator.getBorrowAmountRange();
						printWarn(range);
					}
					break;
				case 1:
					apr = Double.parseDouble(line);
					if (validator.isValidAPRValue(apr) == false) {
						isValidValue = false;
						double range[] = validator.getAPRRange();
						printWarn(range);
					}
					break;
				case 2:
					years = Integer.parseInt(line);
					if (validator.isValidTerm(years) == false) {
						isValidValue = false;
						int range[] = validator.getTermRange();
						printWarnInteger(range);
					}
					break;
				}
			} catch (NumberFormatException e) {
				isValidValue = false;
			}
			if (isValidValue) {
				i++;
			} else {
				print("An invalid value was entered.\n");
			}
		}
		
		try {
			AmortizationSchedule as = new AmortizationSchedule(amount, apr, years);
			as.outputAmortizationSchedule();
		} catch (IllegalArgumentException e) {
			print("Unable to process the values entered. Terminating program.\n");
		}
	}

	private static void printWarn(double[] range) {
		print("Please enter a positive value between " + range[0] + " and " + range[1] + ". ");
	}
	private static void printWarnInteger(int[] range) {
		print("Please enter a positive integer value between " + range[0] + " and " + range[1] + ". ");
	}
}

