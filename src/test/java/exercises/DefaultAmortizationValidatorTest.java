package exercises;

import static org.junit.Assert.*;

import org.junit.Test;

import exercises.amortization.validator.DefaultAmortizationValidator;

public class DefaultAmortizationValidatorTest {
	private static final double VALID_BORROW_AMOUNT = 1001001d;
	private static final double INVALID_BORROW_AMOUNT = -1d;
	private static final double VALID_APR_RANGE_1 = 100d;
	private static final double VALID_APR_RANGE_2 = 0.000001d;
	private static final double VALID_APR_RANGE_3 = 10d;
	private static final double INVALID_APR_RANGE_1 = -12d;
	private static final double INVALID_APR_RANGE_2 = 0d;
	private static final double INVALID_APR_RANGE_3 = 101d;
	private static final int VALID_TERM_RANGE_1 = 1;
	private static final int VALID_TERM_RANGE_2 = 30;
	private static final int VALID_TERM_RANGE_3 = 1000000;
	private static final int INVALID_TERM_RANGE_1 = -12;
	private static final int INVALID_TERM_RANGE_2 = 0;
	private static final int INVALID_TERM_RANGE_3 = 1000001;
	
	@Test
	public void testIsValidBorrowAmount(){
		DefaultAmortizationValidator testSubject = new DefaultAmortizationValidator();
		boolean result = testSubject.isValidBorrowAmount(VALID_BORROW_AMOUNT);
		assertTrue(result);
		result = testSubject.isValidBorrowAmount(INVALID_BORROW_AMOUNT);
		assertFalse(result);
	}
	
	@Test
	public void testIsValidAPRValue(){
		DefaultAmortizationValidator testSubject = new DefaultAmortizationValidator();
		boolean result = testSubject.isValidAPRValue(VALID_APR_RANGE_1);
		assertTrue(result);
		result = testSubject.isValidAPRValue(VALID_APR_RANGE_2);
		assertTrue(result);
		result = testSubject.isValidAPRValue(VALID_APR_RANGE_3);
		assertTrue(result);
		
		result = testSubject.isValidAPRValue(INVALID_APR_RANGE_1);
		assertFalse(result);
		result = testSubject.isValidAPRValue(INVALID_APR_RANGE_2);
		assertFalse(result);
		result = testSubject.isValidAPRValue(INVALID_APR_RANGE_3);
		assertFalse(result);
	}	
	
	@Test
	public void testIsValidTerm(){
		DefaultAmortizationValidator testSubject = new DefaultAmortizationValidator();
		boolean result = testSubject.isValidTerm(VALID_TERM_RANGE_1);
		assertTrue(result);
		result = testSubject.isValidTerm(VALID_TERM_RANGE_2);
		assertTrue(result);
		result = testSubject.isValidTerm(VALID_TERM_RANGE_3);
		assertTrue(result);
		
		result = testSubject.isValidTerm(INVALID_TERM_RANGE_1);
		assertFalse(result);
		result = testSubject.isValidTerm(INVALID_TERM_RANGE_2);
		assertFalse(result);
		result = testSubject.isValidTerm(INVALID_TERM_RANGE_3);
		assertFalse(result);
	}
}
